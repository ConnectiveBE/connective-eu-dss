package eu.europa.esig.dss.pdf.pdfbox;

import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.visible.PDFTemplateStructure;

/**
 * @author Jeroen Verhulst
 */
public class PdfBoxTemplateStructure extends PDFTemplateStructure {

    private PDXObjectImage pdxObjectImage;

    public PDXObjectImage getPdxObjectImage() {
        return pdxObjectImage;
    }

    public void setPdxObjectImage(PDXObjectImage pdxObjectImage) {
        this.pdxObjectImage = pdxObjectImage;
    }
}
