package eu.europa.esig.dss.pades.signature.visible;

import eu.europa.esig.dss.DSSException;
import eu.europa.esig.dss.pades.SignatureImageParameters;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.imageio.*;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Iterator;
import java.util.UUID;

/**
 * A static utilities that helps in creating ImageAndResolution
 * 
 * @author pakeyser
 *
 */
public class ImageUtils {

	private static final int DPI = 300;

	public static InputStream create(final SignatureImageParameters imageParameters) throws IOException {
		if (StringUtils.isNotBlank(imageParameters.getImageData())) {
			return readAndDisplayMetadata(Base64.decodeBase64(imageParameters.getImageData()));
		} else {
			BufferedImage combined = createBackgroundImage(imageParameters);
			return convertToInputStream(combined, DPI);
		}
	}

	private static BufferedImage createBackgroundImage(SignatureImageParameters imageParameters) {
		int textWidth = Double.valueOf(imageParameters.getWidth()).intValue();
		int textHeight = Double.valueOf(imageParameters.getHeight()).intValue();
		BufferedImage combined = new BufferedImage(textWidth, textHeight, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = combined.createGraphics();
		initRendering(g);
		fillBackground(g, textWidth, textHeight, imageParameters.getTextParameters() != null ? imageParameters.getTextParameters().getBackgroundColor() : Color.WHITE);
		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
		g.dispose();
		return combined;
	}

	private static void initRendering(Graphics2D g) {
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);

	}
	private static void fillBackground(Graphics g, final int width, final int heigth, final Color bgColor) {
		g.setColor(bgColor);
		g.fillRect(0, 0, width, heigth);
	}

	public static InputStream readAndDisplayMetadata(byte[] imageDataBytes) throws IOException {

//		Iterator<ImageReader> readers = ImageIO.getImageReadersByFormatName("jpeg");
//		if (!readers.hasNext()) {
//			throw new DSSException("No writer for JPEG found");
//		}

		// pick the first available ImageReader
//		ImageReader reader = readers.next();

//		File image = File.createTempFile(UUID.randomUUID().toString(), ".tmp");
//		FileUtils.writeByteArrayToFile(image, imageDataBytes);
//		ImageInputStream iis = ImageIO.createImageInputStream(image);

		// attach source to the reader
//		reader.setInput(iis, true);

		// read metadata of first image
//		IIOMetadata metadata = reader.getImageMetadata(0);
//		Node asTree = metadata.getAsTree("javax_imageio_jpeg_image_1.0");
//		ImageAndResolution res = readResolution(asTree, );

//		image.delete();
		return new ByteArrayInputStream(imageDataBytes);

	}

//	private static ImageAndResolution readResolution(Node node, InputStream is) {
//		Element root = (Element) node;
//		NodeList elements = root.getElementsByTagName("app0JFIF");
//		Element e = (Element) elements.item(0);
//		int x = Integer.parseInt(e.getAttribute("Xdensity"));
//		int y = Integer.parseInt(e.getAttribute("Ydensity"));
//		return new ImageAndResolution(is, x, y);
//	}

	private static InputStream convertToInputStream(BufferedImage buffImage, int dpi) throws IOException {
		Iterator<ImageWriter> it = ImageIO.getImageWritersByFormatName("jpeg");
		if (!it.hasNext()) {
			throw new DSSException("No writer for JPEG found");
		}
		ImageWriter writer = it.next();

		JPEGImageWriteParam jpegParams = (JPEGImageWriteParam) writer.getDefaultWriteParam();
		jpegParams.setCompressionMode(JPEGImageWriteParam.MODE_EXPLICIT);
		jpegParams.setCompressionQuality(1);

		ImageTypeSpecifier typeSpecifier = ImageTypeSpecifier.createFromBufferedImageType(BufferedImage.TYPE_INT_RGB);
		IIOMetadata metadata = writer.getDefaultImageMetadata(typeSpecifier, jpegParams);

		initDpi(metadata, dpi);

		ByteArrayOutputStream os = new ByteArrayOutputStream();
		ImageOutputStream imageOs = ImageIO.createImageOutputStream(os);
		writer.setOutput(imageOs);
		writer.write(metadata, new IIOImage(buffImage, null, metadata), jpegParams);

		InputStream is = new ByteArrayInputStream(os.toByteArray());
		return is;
	}

	private static void initDpi(IIOMetadata metadata, int dpi) throws IIOInvalidTreeException {
		Element tree = (Element) metadata.getAsTree("javax_imageio_jpeg_image_1.0");
		Element jfif = (Element) tree.getElementsByTagName("app0JFIF").item(0);
		jfif.setAttribute("Xdensity", Integer.toString(dpi));
		jfif.setAttribute("Ydensity", Integer.toString(dpi));
		jfif.setAttribute("resUnits", "1"); // density is dots per inch
		metadata.setFromTree("javax_imageio_jpeg_image_1.0", tree);
	}
}
