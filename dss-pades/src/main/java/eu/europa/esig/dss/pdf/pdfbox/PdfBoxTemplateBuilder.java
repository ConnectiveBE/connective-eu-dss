package eu.europa.esig.dss.pdf.pdfbox;

import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.visible.PDFTemplateBuilder;

import java.awt.geom.AffineTransform;
import java.io.IOException;

/**
 * @author Jeroen Verhulst
 */
public interface PdfBoxTemplateBuilder extends PDFTemplateBuilder {

    /**
     * Creates Image form
     *
     * @param imageFormResources
     * @param innerFormResource
     * @param imageFormStream
     * @param formrect
     * @param affineTransform
     * @param img
     * @throws IOException
     */
    void createImageForm(PDResources imageFormResources, PDResources innerFormResource,
                                PDStream imageFormStream, PDRectangle formrect, AffineTransform affineTransform, PDXObjectImage img)
            throws IOException;
}
