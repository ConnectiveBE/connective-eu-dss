package eu.europa.esig.dss.asic;

import eu.europa.esig.dss.AbstractSignatureParameters;
import eu.europa.esig.dss.DigestAlgorithm;
import eu.europa.esig.dss.SignatureLevel;

import static eu.europa.esig.dss.DigestAlgorithm.SHA1;

public class ASiCSignatureParameters extends AbstractSignatureParameters {

	/**
	 * The object representing the parameters related to ASiC from of the signature.
	 */
	private ASiCParameters aSiCParams = new ASiCParameters();

	private DigestAlgorithm signedPropertiesDigestAlgorithm  = SHA1;
	private DigestAlgorithm unsignedPropertiesDigestAlgorithm  = SHA1;
	private DigestAlgorithm signedInfoDigestAlgorithm  = SHA1;

	/**
	 * ds:CanonicalizationMethod indicates the canonicalization algorithm: Algorithm="..." for SignedInfo.
	 */
	private String signedInfoCanonicalizationMethod;

	/**
	 * ds:CanonicalizationMethod indicates the canonicalization algorithm: Algorithm="..." for SignedProperties.
	 */
	private String signedPropertiesCanonicalizationMethod;

	/**
	 * ds:CanonicalizationMethod indicates the canonicalization algorithm: Algorithm="..." for UnsignedProperties.
	 */
	private String unsignedPropertiesCanonicalizationMethod;

	private boolean en319132 = false;

	public ASiCParameters aSiC() {
		if (aSiCParams == null) {
			aSiCParams = new ASiCParameters();
		}
		return aSiCParams;
	}

	@Override
	public void setSignatureLevel(SignatureLevel signatureLevel) {
		super.setSignatureLevel(signatureLevel);
		ASiCParameters aSiC = aSiC();
		aSiC.containerForm = signatureLevel.getSignatureForm();
	}

	public DigestAlgorithm getSignedPropertiesDigestAlgorithm() {
		return signedPropertiesDigestAlgorithm;
	}

	public void setSignedPropertiesDigestAlgorithm(DigestAlgorithm signedPropertiesDigestAlgorithm) {
		this.signedPropertiesDigestAlgorithm = signedPropertiesDigestAlgorithm;
	}

	public DigestAlgorithm getUnsignedPropertiesDigestAlgorithm() {
		return unsignedPropertiesDigestAlgorithm;
	}

	public void setUnsignedPropertiesDigestAlgorithm(DigestAlgorithm unsignedPropertiesDigestAlgorithm) {
		this.unsignedPropertiesDigestAlgorithm = unsignedPropertiesDigestAlgorithm;
	}

	public DigestAlgorithm getSignedInfoDigestAlgorithm() {
		return signedInfoDigestAlgorithm;
	}

	public void setSignedInfoDigestAlgorithm(DigestAlgorithm signedInfoDigestAlgorithm) {
		this.signedInfoDigestAlgorithm = signedInfoDigestAlgorithm;
	}

	public String getSignedInfoCanonicalizationMethod() {
		return signedInfoCanonicalizationMethod;
	}

	public void setSignedInfoCanonicalizationMethod(String signedInfoCanonicalizationMethod) {
		this.signedInfoCanonicalizationMethod = signedInfoCanonicalizationMethod;
	}

	public String getSignedPropertiesCanonicalizationMethod() {
		return signedPropertiesCanonicalizationMethod;
	}

	public void setSignedPropertiesCanonicalizationMethod(String signedPropertiesCanonicalizationMethod) {
		this.signedPropertiesCanonicalizationMethod = signedPropertiesCanonicalizationMethod;
	}

	public String getUnsignedPropertiesCanonicalizationMethod() {
		return unsignedPropertiesCanonicalizationMethod;
	}

	public void setUnsignedPropertiesCanonicalizationMethod(String unsignedPropertiesCanonicalizationMethod) {
		this.unsignedPropertiesCanonicalizationMethod = unsignedPropertiesCanonicalizationMethod;
	}

	public boolean isEn319132() {
		return en319132;
	}

	public void setEn319132(boolean en319132) {
		this.en319132 = en319132;
	}
}