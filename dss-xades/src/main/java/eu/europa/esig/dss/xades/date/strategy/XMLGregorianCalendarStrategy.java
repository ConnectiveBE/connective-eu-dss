package eu.europa.esig.dss.xades.date.strategy;

import eu.europa.esig.dss.xades.DSSXMLUtils;
import eu.europa.esig.dss.xades.date.XMLDateStrategy;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;

/**
 * @author Jeroen Verhulst
 */
public class XMLGregorianCalendarStrategy implements XMLDateStrategy {

    @Override
    public String getXmlDate(Date date) {
        String result = "";
        if(date != null) {
            final XMLGregorianCalendar xmlGregorianCalendar = DSSXMLUtils.createXMLGregorianCalendar(date);
            result = xmlGregorianCalendar.toXMLFormat();
        }

        return result;
    }
}
