package eu.europa.esig.dss.xades;

import java.util.List;

import org.w3c.dom.Document;

import eu.europa.esig.dss.AbstractSignatureParameters;
import eu.europa.esig.dss.DigestAlgorithm;

import static eu.europa.esig.dss.DigestAlgorithm.SHA1;

public class XAdESSignatureParameters extends AbstractSignatureParameters {

	ProfileParameters context;

	private DigestAlgorithm signedPropertiesDigestAlgorithm  = SHA1;
	private DigestAlgorithm unsignedPropertiesDigestAlgorithm  = SHA1;
	private DigestAlgorithm signedInfoDigestAlgorithm  = SHA1;

	/**
	 * ds:CanonicalizationMethod indicates the canonicalization algorithm: Algorithm="..." for SignedInfo.
	 */
	private String signedInfoCanonicalizationMethod;

	/**
	 * ds:CanonicalizationMethod indicates the canonicalization algorithm: Algorithm="..." for SignedProperties.
	 */
	private String signedPropertiesCanonicalizationMethod;

	/**
	 * ds:CanonicalizationMethod indicates the canonicalization algorithm: Algorithm="..." for UnsignedProperties.
	 */
	private String unsignedPropertiesCanonicalizationMethod;

	private List<DSSReference> dssReferences;

	private String xPathLocationString;
	
	private boolean en319132 = false;

	/**
	 *	This attribute is used to inject ASiC root (inclusive canonicalization)
	 */
	private Document rootDocument;

	public DigestAlgorithm getSignedPropertiesDigestAlgorithm() {
		return signedPropertiesDigestAlgorithm;
	}

	public void setSignedPropertiesDigestAlgorithm(DigestAlgorithm signedPropertiesDigestAlgorithm) {
		this.signedPropertiesDigestAlgorithm = signedPropertiesDigestAlgorithm;
	}

	public DigestAlgorithm getUnsignedPropertiesDigestAlgorithm() {
		return unsignedPropertiesDigestAlgorithm;
	}

	public void setUnsignedPropertiesDigestAlgorithm(DigestAlgorithm unsignedPropertiesDigestAlgorithm) {
		this.unsignedPropertiesDigestAlgorithm = unsignedPropertiesDigestAlgorithm;
	}

	public DigestAlgorithm getSignedInfoDigestAlgorithm() {
		return signedInfoDigestAlgorithm;
	}

	public void setSignedInfoDigestAlgorithm(DigestAlgorithm signedInfoDigestAlgorithm) {
		this.signedInfoDigestAlgorithm = signedInfoDigestAlgorithm;
	}

	public String getUnsignedPropertiesCanonicalizationMethod() {
		return unsignedPropertiesCanonicalizationMethod;
	}

	public void setUnsignedPropertiesCanonicalizationMethod(String unsignedPropertiesCanonicalizationMethod) {
		this.unsignedPropertiesCanonicalizationMethod = unsignedPropertiesCanonicalizationMethod;
	}

	/**
	 * @return the canonicalization algorithm to be used when dealing with SignedInfo.
	 */
	public String getSignedInfoCanonicalizationMethod() {
		return signedInfoCanonicalizationMethod;
	}

	/**
	 * Set the canonicalization algorithm to be used when dealing with SignedInfo.
	 *
	 * @param signedInfoCanonicalizationMethod the canonicalization algorithm to be used when dealing with SignedInfo.
	 */
	public void setSignedInfoCanonicalizationMethod(final String signedInfoCanonicalizationMethod) {
		this.signedInfoCanonicalizationMethod = signedInfoCanonicalizationMethod;
	}

	/**
	 * @return the canonicalization algorithm to be used when dealing with SignedProperties.
	 */
	public String getSignedPropertiesCanonicalizationMethod() {
		return signedPropertiesCanonicalizationMethod;
	}

	/**
	 * Set the canonicalization algorithm to be used when dealing with SignedProperties.
	 *
	 * @param signedPropertiesCanonicalizationMethod the canonicalization algorithm to be used when dealing with SignedInfo.
	 */
	public void setSignedPropertiesCanonicalizationMethod(final String signedPropertiesCanonicalizationMethod) {
		this.signedPropertiesCanonicalizationMethod = signedPropertiesCanonicalizationMethod;
	}

	public List<DSSReference> getReferences() {
		return dssReferences;
	}

	public void setReferences(List<DSSReference> references) {
		this.dssReferences = references;
	}

	public String getXPathLocationString() {
		return xPathLocationString;
	}

	/**
	 * Defines the area where the signature will be added (XAdES Enveloped)
	 * @param xPathLocationString the xpath location of the signature
	 */
	public void setXPathLocationString(String xPathLocationString) {
		this.xPathLocationString = xPathLocationString;
	}

	public Document getRootDocument() {
		return rootDocument;
	}

	public void setRootDocument(Document rootDocument) {
		this.rootDocument = rootDocument;
	}

	public ProfileParameters getContext() {
		if (context == null) {
			context = new ProfileParameters();
		}
		return context;
	}

	public boolean isEn319132() {
		return en319132;
	}

	public void setEn319132(boolean en319132) {
		this.en319132 = en319132;
	}
	
}
