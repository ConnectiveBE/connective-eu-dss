/**
 * DSS - Digital Signature Services
 * Copyright (C) 2015 European Commission, provided under the CEF programme
 *
 * This file is part of the "DSS - Digital Signature Services" project.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
package eu.europa.esig.dss.xades.signature;

import eu.europa.esig.dss.*;
import eu.europa.esig.dss.validation.CertificateVerifier;
import eu.europa.esig.dss.xades.DSSReference;
import eu.europa.esig.dss.xades.DSSTransform;
import eu.europa.esig.dss.xades.DSSXMLUtils;
import eu.europa.esig.dss.xades.XAdESSignatureParameters;
import eu.europa.esig.dss.xades.date.XMLDateStrategy;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.xpath.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class handles the specifics of the enveloping XML signature
 *
 */
class EnvelopingSignatureBuilder extends XAdESSignatureBuilder {

	private static final byte[] xmlPreamble = new byte[] { '<', '?', 'x', 'm', 'l' };
	private static final byte[] xmlUtf8 = new byte[] { -17, -69, -65, '<', '?' };

	/**
	 * The default constructor for EnvelopingSignatureBuilder. The enveloped signature uses by default the inclusive
	 * method of canonicalization.
	 * 
	 * @param params
	 *            The set of parameters relating to the structure and process of the creation or extension of the
	 *            electronic signature.
	 * @param origDoc
	 *            The original document to sign.
	 * @param certificateVerifier
	 */
	public EnvelopingSignatureBuilder(final XAdESSignatureParameters params, final DSSDocument origDoc, final CertificateVerifier certificateVerifier, final XMLDateStrategy xmlDateStrategy) {

		super(params, origDoc, certificateVerifier, xmlDateStrategy);
		setCanonicalizationMethods(params, CanonicalizationMethod.INCLUSIVE);
	}

	@Override
	protected List<DSSReference> createDefaultReferences() {

		final List<DSSReference> references = new ArrayList<DSSReference>();
		final List<DSSTransform> transforms = new ArrayList<DSSTransform>();

		// CONNECTIVE CHANGE
		if(!supportsXmlIntegration(detachedDocument, true)) {
			final DSSTransform transform = new DSSTransform();
			transform.setAlgorithm(CanonicalizationMethod.BASE64);
			transforms.add(transform);
		}

		DSSDocument document = detachedDocument;
		int referenceId = 1;
		do {
			// <ds:Reference Id="signed-data-ref" Type="http://www.w3.org/2000/09/xmldsig#Object"
			// URI="#signed-data-idfc5ff27ee49763d9ba88ba5bbc49f732">
			final DSSReference reference = new DSSReference();
			reference.setId("r-id-" + referenceId);
			reference.setType(HTTP_WWW_W3_ORG_2000_09_XMLDSIG_OBJECT);
			reference.setUri("#o-id-" + referenceId);
			reference.setContents(document);
			reference.setDigestMethodAlgorithm(params.getSignedInfoDigestAlgorithm());

			// CONNECTIVE CHANGE
			if(CollectionUtils.isNotEmpty(transforms)) {
				reference.setTransforms(transforms);
			}

			references.add(reference);

			referenceId++;
			document = document.getNextDocument();
		} while (document != null);

		return references;
	}

	@Override
	protected DSSDocument transformReference(final DSSReference reference) {
		if(supportsXmlIntegration(detachedDocument, false)) {
			DSSDocument dssDocument = reference.getContents();
			List<DSSTransform> transforms = reference.getTransforms();
			if (CollectionUtils.isEmpty(transforms)) { // no transformations? use default: http://www.w3.org/TR/2001/REC-xml-c14n-20010315
				transforms = new ArrayList<>();
				DSSTransform dssTransform = new DSSTransform();
				dssTransform.setAlgorithm(CanonicalizationMethod.INCLUSIVE);
				transforms.add(dssTransform);
			} else {
				if(Transform.BASE64.equals(transforms.get(0).getAlgorithm())) {
					return reference.getContents();
				}
			}

			final String uri = reference.getUri();
			// Check if the reference is related to the whole document
			if (StringUtils.isNotBlank(uri) && uri.startsWith("#") && !isXPointer(uri)) {
				Document doc = DSSXMLUtils.buildDOM(reference.getContents());
				DSSXMLUtils.recursiveIdBrowse(doc.getDocumentElement());
				final String uri_id = uri.substring(1);
				try {
					XPathFactory xPathfactory = XPathFactory.newInstance();
					XPath xpath = xPathfactory.newXPath();

					// case insensitive id check
					XPathExpression expr = xpath.compile(
							"//*[@id='" + uri_id + "' " +
									"or @Id='" + uri_id + "' " +
									"or @ID='" + uri_id + "' " +
									"or @iD='" + uri_id + "']"
					);

					Node nodeToTransform = (Node) expr.evaluate(doc, XPathConstants.NODE);
					if(nodeToTransform != null) { // inline element found: only the fragment should be used for calculating the digest of the reference
						if(!supportsXmlIntegration(detachedDocument, true)) {
							throw new DSSException("XPath expression only supported for XML documents withour preamble.");
						}
						byte[] transformedReferenceBytes = applyTransformations(dssDocument, transforms, nodeToTransform);
						return new InMemoryDocument(transformedReferenceBytes);
					} else { // the complete document inside a ds:Object should be used to calculate the digest of the reference
						Node imported = documentDom.importNode(doc.getDocumentElement(), true);
						final Element objectDom = DSSXMLUtils.addElement(documentDom, documentDom.getDocumentElement(), XMLSignature.XMLNS, DS_OBJECT);
						objectDom.appendChild(imported);
						objectDom.setAttribute(ID, reference.getUri().substring(1));
						byte[] transformedReferenceBytes = applyTransformations(dssDocument, transforms, objectDom);
						InMemoryDocument inMemoryDocument = new InMemoryDocument(transformedReferenceBytes);
						documentDom.getDocumentElement().removeChild(objectDom);
						return inMemoryDocument;
					}
				} catch (XPathExpressionException e) {
					throw new DSSException("Unable to select the element with id " + uri_id + ".", e);
				}
			}
		}

		return reference.getContents();
	}

	/**
	 * Adds signature value to the signature and returns XML signature (InMemoryDocument)
	 *
	 * @param signatureValue
	 * @return
	 * @throws DSSException
	 */
	@Override
	public DSSDocument signDocument(final byte[] signatureValue) throws DSSException {
		if (!built) {
			build();
		}

		final EncryptionAlgorithm encryptionAlgorithm = params.getEncryptionAlgorithm();
		final byte[] signatureValueBytes = DSSSignatureUtils.convertToXmlDSig(encryptionAlgorithm, signatureValue);
		final String signatureValueBase64Encoded = Base64.encodeBase64String(signatureValueBytes);
		final Text signatureValueNode = documentDom.createTextNode(signatureValueBase64Encoded);
		signatureValueDom.appendChild(signatureValueNode);

		final List<DSSReference> references = params.getReferences();
		for (final DSSReference reference : references) {

			// <ds:Object>
			Element objectDom = null;

			// CONNECTIVE CHANGE
			if(supportsXmlIntegration(reference.getContents(), true)) {
				Document doc = DSSXMLUtils.buildDOM(reference.getContents()); // the complete document inside ds:Object tag

				Node imported = documentDom.importNode(doc.getDocumentElement(), true);
				objectDom = DSSXMLUtils.addElement(documentDom, signatureDom, XMLSignature.XMLNS, DS_OBJECT);
				final String id = reference.getUri().substring(1);
				if(StringUtils.startsWith(id, "o-id")) {
					objectDom.setAttribute(ID, id);
				}
				objectDom.appendChild(imported);
			} else if (!supportsXmlIntegration(reference.getContents(), true)) {
				String encodedOriginalDocument = Base64.encodeBase64String(DSSUtils.toByteArray(reference.getContents()));
				objectDom = DSSXMLUtils.addTextElement(documentDom, signatureDom, XMLSignature.XMLNS, DS_OBJECT, encodedOriginalDocument);
				final String id = reference.getUri().substring(1);
				objectDom.setAttribute(ID, id);
			}
		}

		byte[] documentBytes = DSSXMLUtils.transformDomToByteArray(documentDom);
		final InMemoryDocument inMemoryDocument = new InMemoryDocument(documentBytes);
		inMemoryDocument.setMimeType(MimeType.XML);
		return inMemoryDocument;
	}

	private boolean supportsXmlIntegration(DSSDocument dssDocument, boolean checkPreamble) {
		final String dssDocumentName = dssDocument.getName();
		if ((dssDocumentName != null) && MimeType.XML.equals(MimeType.fromFileName(dssDocumentName))) {
			if(checkPreamble) {
				int headerLength = 500;
				byte[] preamble = new byte[headerLength];
				DSSUtils.readToArray(dssDocument, headerLength, preamble);
				return !isXmlPreamble(preamble);
			} else {
				return true;
			}
		}
		return false;
	}

	private boolean isXmlPreamble(byte[] preamble) {
		byte[] startOfPramble = ArrayUtils.subarray(preamble, 0, xmlPreamble.length);
		return Arrays.equals(startOfPramble, xmlPreamble) || Arrays.equals(startOfPramble, xmlUtf8);
	}

	@Override
	protected Document buildRootDocumentDom() {
		if (params.getRootDocument() != null) {
			return params.getRootDocument();
		}
		return DSSXMLUtils.buildDOM();
	}

	@Override
	protected Node getParentNodeOfSignature() {
		if (params.getRootDocument() != null) {
			return documentDom.getDocumentElement();
		}
		return documentDom;
	}
}