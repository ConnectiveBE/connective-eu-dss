package eu.europa.esig.dss.xades.date;

import java.util.Date;

/**
 * Created by Jeroen on 16/01/2017.
 */
public interface XMLDateStrategy {

    String getXmlDate(Date date);
}
