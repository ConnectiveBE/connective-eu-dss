package eu.europa.esig.dss.xades.date.strategy;

import eu.europa.esig.dss.xades.date.XMLDateStrategy;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Jeroen Verhulst
 */
public class SimpleDateFormatStrategy implements XMLDateStrategy {

    private String format;

    public SimpleDateFormatStrategy(String format) {
        this.format = format;
    }

    @Override
    public String getXmlDate(Date date) {
        String result = "";
        if(date != null) {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            result = sdf.format(date);
        }

        return result;
    }
}
