## Connective EU Digital Signature Service

This is Connective's fork of the official repository for project DSS: https://joinup.ec.europa.eu/asset/sd-dss/description.

# Issue Tracker

Use the JIRA project on: https://jira.connective.eu/browse/DSS.

# Maven repository

The release is published on Artifactory repository: http://artifactory.connective.local/artifactory/product-release-local/